import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-component-list',
	templateUrl: './component-list.component.html',
	styleUrls: ['./component-list.component.less'],
})
export class ComponentsListComponent implements OnInit {

	// tslint:disable-next-line: no-empty
	constructor() {}

	// tslint:disable-next-line: no-empty
	public ngOnInit(): void {}
}
