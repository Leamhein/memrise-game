import { SharedModule } from '../shared/shared.module';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { ComponentsListComponent } from './component-list/component-list.component';

@NgModule({
	imports: [
		BrowserModule,
		HttpClientModule,
		SharedModule,
	],
	declarations: [
		ComponentsListComponent,
	],
	exports: [ComponentsListComponent],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class ComponentsModule {}
