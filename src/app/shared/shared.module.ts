import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

@NgModule({
	imports: [
		CommonModule,
		FormsModule,
	],
	declarations: [
	],
	providers: [
	],
	exports: [
		FormsModule,
	],
	schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class SharedModule { }
